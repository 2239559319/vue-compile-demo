const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { VueLoaderPlugin } = require('vue-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { join } = require('path');

/**
 * @type {import('webpack').Configuration}
 */
const config = {
  entry: './src/index.ts',
  output: {
    filename: 'main.js',
    path: join(__dirname, 'dist')
  },

  devtool: 'source-map',
  mode: 'production',
  optimization: {
    minimize: false
  },
  resolve: {
    extensions: ['.ts', '.vue', '.js']
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
        ]
      },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
          appendTsSuffixTo: ['\\.vue$'],  
        }
      }
    ]
  },
  externals: {
    vue: 'Vue'
  },
  plugins: [
    new MiniCssExtractPlugin(),
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      template: join(__dirname, 'public', 'index.html')
    })
  ]
};

module.exports = config;
